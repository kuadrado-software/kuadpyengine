import pygame


class MoveRequest:
    """
    A state structure holding boolean values for directions up down right and left.
    The subscrption field allows to take only certain directions in account.
    """

    def __init__(self):
        self.up = False
        self.right = False
        self.down = False
        self.left = False

        self.subscriptions = {
            "up": pygame.K_UP,
            "down": pygame.K_DOWN,
            "right": pygame.K_RIGHT,
            "left": pygame.K_LEFT
        }

        self.updateSubscriptionKeysReverseMap()

    def setState(self, key: int, value):
        """
        Update the internal state for one direction if the direction has been subscribed.
        """
        if key not in self.subscriptionKeysReverseMap.keys():
            return

        direction = self.subscriptionKeysReverseMap[key]
        if direction in self.subscriptions.keys() and key == self.subscriptions.get(direction):
            setattr(self, direction, value)

    def subscribe(self, subscriptions: dict):
        """
        Set the self.subscriptions field. Only directions provided in this dictionary will be 
        taken in account in the setState method. If this method is never called, default will be to subscribe for up down left and right.
        """
        self.subscriptions = subscriptions
        self.updateSubscriptionKeysReverseMap()

    def updateSubscriptionKeysReverseMap(self):
        """
        If the self.subscriptions field is updated, this method will be called to update
        a reverse map of the subscriptions for convenience.
        """
        rmap = dict()
        for key in self.subscriptions.keys():
            rmap[self.subscriptions[key]] = key

        self.subscriptionKeysReverseMap = rmap
