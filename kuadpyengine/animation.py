import pygame
from .rectangle import Rectangle
from .frame_rate import FrameRateController


class Animation:
    """
    Provides an abstraction of an animation. Holds convenient animation data like frame_count, size, name, collider, etc.
    """

    def __init__(self, animationData: dict):
        self.name = animationData["name"]
        self.frame_count = animationData["frame_count"]
        self.fps = animationData["fps"]
        self.dimensions = animationData["dimensions"]
        self.collider = Rectangle(pygame.Rect(animationData["collider"]))
        self.flip = animationData.get("flip", (False, False))
        self.image = pygame.transform.flip(
            animationData["image"], self.flip[0], self.flip[1])

        self.currentFrame = 0
        self.fpsController = FrameRateController(self.fps or 50)

    def updateFrame(self):
        """
        Update the current frame index to use.
        """
        if self.fpsController.nextFrameReady() and self.frame_count > 1 and self.fps > 0:
            self.currentFrame = (
                (self.currentFrame + 1) &
                ~(self.currentFrame + 1 >= self.frame_count and ~0)
            )
            """
            if self.currentFrame + 1 >= self.frame_count:
                self.currentFrame = 0
            else:
                self.currentFrame = self.currentFrame + 1
            """

    def getFramePosition(self):
        """
        Returns the sprite position regarding the animation frame index and the animation size
        """
        return self.currentFrame * self.dimensions[0]

    def getFrameRect(self):
        """
        Returns a rectangle's coordinates from the current animation frame.
        """
        w, h = self.dimensions
        return (self.getFramePosition(), 0, w, h)
