import pygame
import sys
from .move_request import MoveRequest

arr_keys_reverse_map = dict()
arr_keys_reverse_map[pygame.K_UP] = "up"
arr_keys_reverse_map[pygame.K_RIGHT] = "right"
arr_keys_reverse_map[pygame.K_DOWN] = "down"
arr_keys_reverse_map[pygame.K_LEFT] = "left"


class EventsHandler:
    """
    Provides a set of methods to handle window events.
    """

    @staticmethod
    def handleEvtQueue(events, moveRequests: MoveRequest):
        """
        Parse the window event queue. If an event is a keyboard event, it is processed in the next method: handleKeyEvent,
        A state of move requests: MoveRequest must be provided in the arguments
        """
        for e in events:
            if e.type == pygame.QUIT or (e.type == pygame.KEYDOWN and e.key == pygame.K_ESCAPE):
                sys.exit()
            elif (e.type == pygame.KEYDOWN or e.type == pygame.KEYUP):
                EventsHandler.handleKeyEvent(e, moveRequests)

    @staticmethod
    def handleKeyEvent(event: pygame.event, moveRequests: MoveRequest):
        """
        Update the state of a MoveRequest object with a keyboard event.
        """
        for req in moveRequests:
            req.setState(
                event.key,
                event.type == pygame.KEYDOWN
            )
