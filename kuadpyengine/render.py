import pygame
from typing import Tuple
from .sprite import Sprite


class Render:
    """
    Provides a set of static methods to wrap pygame rendering system.
    """
    @staticmethod
    def drawGame(surface: pygame.Surface, sprites: Tuple[Sprite, ...], backgroundImage: pygame.Surface, cameraOffset=(0, 0)):
        """
        This is the only public method of the class, it must be called with a background image and a set of sprites to draw.
        An optional camera offset position can be provided.
        """
        Render.__drawBackground(surface, backgroundImage, cameraOffset)
        Render.__drawSprites(surface, sprites, cameraOffset)
        pygame.display.flip()

    @staticmethod
    def __drawBackground(surface, image, cameraOffset):
        """
        Draws a background image on the window.
        """
        surface.blit(
            image, (0 + cameraOffset[0], 0 + cameraOffset[1]), image.get_rect())

    @staticmethod
    def __drawSprites(surface, sprites, cameraOffset):
        """
        Draw a set of sprites on the windows.
        """
        for sprite in sprites:
            surface.blit(  # Les pixels sont passé à la surface grâce à la méthode blit()
                sprite.animation.image,
                (sprite.getPosition()[0] + cameraOffset[0],
                 sprite.getPosition()[1] + cameraOffset[1]),
                sprite.animation.getFrameRect()
            )
