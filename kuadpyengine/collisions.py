from math import copysign
from .rectangle import Rectangle


class CollisionHandler:
    """
    Provides a set of static functions for different cases of collision detection
    """

    @staticmethod
    def rectCollidesOutsideRect(r1: Rectangle, r2: Rectangle) -> bool:
        """
        Returns true if rectangle r1 intersects with rectangle r2
        """
        l1, t1, r1, b1 = r1.ltrb()
        l2, t2, r2, b2 = r2.ltrb()
        return (l1 <= r2) & (r1 >= l2) & (t1 <= b2) & (b1 >= t2)

    @staticmethod
    def rectCollidesInsideRect(r1: Rectangle, r2: Rectangle) -> bool:
        """
        Returns true if rectangle r1 intersects with one of rectangle's r2 edges, assuming r1 is inside r2.
        """
        l1, t1, r1, b1 = r1.ltrb()
        l2, t2, r2, b2 = r2.ltrb()
        return (l1 <= l2) | (r1 >= r2) | (t1 <= t2) | (b1 >= b2)

    @staticmethod
    def rectBounceInsideRect(r1: Rectangle, r2: Rectangle) -> tuple:
        """
        Returns a 2D unit vector of rectangle r1 bouncing inside rectangle r2.
        If there is no collision detected, the vector will be 0,0.
        """
        l1, t1, r1, b1 = r1.ltrb()
        l2, t2, r2, b2 = r2.ltrb()

        return (
            (l1 <= l2) | -(r1 >= r2),
            (t1 <= t2) | -(b1 >= b2)
        )

    @staticmethod
    def rectBounceOutsideRect(r1: Rectangle, r2: Rectangle) -> tuple:
        """
        Returns a 2D unit vector of rectangle r1 bouncing against rectangle r2.
        This method assume that r1 is colliding r2 so it must be checked before.
        """
        l1, t1, r1, b1 = r1.ltrb()
        l2, t2, r2, b2 = r2.ltrb()

        return (
            -(l1 < l2) | (r1 > r2),
            -(t1 < t2) | (b1 > b2)
        )

    @staticmethod
    def rectCollideInertOusiteRect(r1: Rectangle, r1SpeedVector: tuple, r2: Rectangle) -> tuple:
        """
        Returns a 2D unit vector for an 100% unelastic response of r1 colliding against r2.
        The initial velocity vector of r1 must be provided in the arguments.
        """
        l1, t1, r1, b1 = r1.ltrb()
        l2, t2, r2, b2 = r2.ltrb()
        r1vx, r1vy = r1SpeedVector
        vx = copysign(1, r1vx) if r1vx != 0 else 0
        vy = copysign(1, r1vy) if r1vy != 0 else 0
        if ((l1 < l2) & (r1vx > 0)) | ((r1 > r2) & (r1vx < 0)):
            vx = 0
        if ((t1 < t2) & (r1vy > 0)) | ((b1 > b2) & (r1vy < 0)):
            vy = 0
        return (vx, vy)

    @staticmethod
    def rectCollideInertInsideRect(r1: Rectangle, r1SpeedVector: tuple, r2: Rectangle) -> tuple:
        """
        Returns a 2D unit vector for 100% unelastic response of r1 colliding against r2 edges from inside.
        Initial r1 velocity vector of r1 must be provided in the arguments.
        """
        l1, t1, r1, b1 = r1.ltrb()
        l2, t2, r2, b2 = r2.ltrb()
        r1vx, r1vy = r1SpeedVector
        vx = copysign(1, r1vx) if r1vx != 0 else 0
        vy = copysign(1, r1vy) if r1vy != 0 else 0
        if ((l1 <= l2) & (r1vx < 0)) | ((r1 >= r2) & (r1vx > 0)):
            vx = 0
        if ((t1 <= t2) & (r1vy < 0)) | ((b1 >= b2) & (r1vy > 0)):
            vy = 0
        return (vx, vy)
