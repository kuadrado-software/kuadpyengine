import pygame
import os
import json
from .animation import Animation
from .sprite import Sprite


class ResourcesManager:
    """
    A ResourceManager instance is responsible of loading the animations resources and holds them in memory 
    as ready to use data structures.
    """

    def __init__(self):
        self.animations: dict
        self.sounds: tuple

    def loadAnimations(self):
        """
        Loads the project's animation resources from the resource/animations/index.json, 
        creates a dictionary of Animations instances from the files data and write them in the 
        self.animations field.
        """
        animationsIndexFile = open(os.path.join(
            "resources", "animations", "index.json"))

        animsIndex = json.load(animationsIndexFile)

        animations = dict()
        for key in animsIndex.keys():
            animations[key] = (
                [
                    Animation(
                        dict(animData, **{
                            "image": pygame.image.load(
                                os.path.join("resources", "animations",
                                             key, animData["file"])
                            )
                        })
                    ) for animData in animsIndex[key]
                ]
            )
        self.animations = animations

    def getAnimationSet(self, key):
        """
        Returns a list of animations from the self.animations dict for the given key.
        """
        return self.animations.get(key)
