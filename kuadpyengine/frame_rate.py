from time import time


class FrameRateController:
    """
    Provides a set of methods to control the frame rate of a game loop.
    """

    def __init__(self, fps):
        """
        An instance of FrameRateController will be initiallized with a frame per second value,
        as the "interval" attribute.
        """
        self.tframe = FrameRateController.getTime()
        self.interval = 1000 / fps

    @staticmethod
    def getTime():
        """
        Returns current time in milliseconds.
        """
        return int(time() * 1000)

    def nextFrameReady(self) -> bool:
        """
        Returns true if self.interval is elapsed
        """
        now = FrameRateController.getTime()
        elpased = now - self.tframe
        ready = elpased >= self.interval
        if ready:
            self.tframe = now - (elpased % self.interval)
        return ready
