import pygame
from math import sqrt
from typing import Tuple

from .rectangle import Rectangle
from .move_request import MoveRequest
from .collisions import CollisionHandler
from .vectors import getUnitVec
from .animation import Animation


class Sprite:
    """
    A sprite abstraction holding data for animation, position, physics configuration, etc.
    Sprite is an abstract class and must be extended in order to create a real sprite.
    """

    def __init__(self, animationSet: Tuple[Animation, ...]):
        """
        All the field are declared with their type but not initialized.
        They must be initialized in the extended class.
        """
        self.animation: Animation  # The currently used animation

        # A reference of the set of animations available for this sprite.
        self.animationSet = animationSet

        self.boundingBox: Rectangle

        self.speed: float  # Speed in pixels par frame
        self.speedVector = (0.0, 0.0)
        self.moveRequestState: MoveRequest

    def getPosition(self):
        """
        Returns the instance's bounding box position
        """
        return self.boundingBox.getPosition()

    def getCenterPosition(self):
        """
        Returns the instance's bounding box center position
        """
        pos = self.getPosition()
        size = self.boundingBox.size()
        return (
            pos[0] + size[0] / 2,
            pos[1] + size[1] / 2
        )

    def setPosition(self, x, y):
        """
        Update the instance's bounding box position.
        """
        return self.boundingBox.setPosition(x, y)

    def setMoveRequestState(self, state: MoveRequest):
        """
        Update the instance's move requests state.
        """
        self.moveRequestState = state

    def setSpeed(self, speed: int):
        """
        Update the instance's speed value in frames per second.
        """
        self.speed = speed

    def setSpeedVector(self, unitVector):
        """
        Update the instance's velocity vector.
        The vector given as an argument must be of unit length.
        """
        self.speedVector = self.getSpeedVecFromUnitVec(unitVector)

    def setAnimation(self, key):
        """
        Updates the instance current animation.
        """
        for anim in self.animationSet:
            if anim.name == key:
                self.animation = anim
                self.updateBoundingBox()
                return

    def updateBoundingBox(self):
        """
        Recalculates the instance's bounding box coordinates from the current animation data.
        """
        _ix, _iy, iw, ih = self.animation.getFrameRect()
        px, py = self.boundingBox.getPosition() if hasattr(
            self, "boundingBox") else (0.0, 0.0)
        self.boundingBox = Rectangle(pygame.Rect((px, py, iw, ih)))

    def getSpeedVecFromUnitVec(self, vector: tuple) -> tuple:
        speedVec = (
            vector[0] * self.speed,
            vector[1] * self.speed
        )
        return speedVec

    def move(self):
        """
        Applys the instance's velocity vector state on its bounding box position.
        """
        (x, y) = self.boundingBox.getPosition()
        (vx, vy) = self.speedVector
        self.boundingBox.setPosition(x + vx, y + vy)

    def applyBounceVector(self, vector: tuple):
        """
        Updates the instance's velocity vector from a collision response vector.
        """
        vx, vy = self.speedVector
        self.setSpeedVector(getUnitVec((
            vector[0] * abs(vx) or vx,
            vector[1] * abs(vy) or vy
        )))

    def applyMoveVector(self, vector: tuple):
        """
        Updates the instance's velocity vector from a move request vector.
        """
        self.setSpeedVector(vector)

    def getCollider(self) -> Rectangle:
        """
        Returns the current animation's collider's data.
        """
        cx, cy, cw, ch = self.animation.collider.xywh()
        ax, ay = self.getPosition()
        return Rectangle(pygame.Rect(ax + cx, ay + cy, cw, ch))

    def bounceInside(self, rect: Rectangle):
        """
        Calls a collision detection between the instance's bounding box and another rectangle from inside.
        Then updates the instance's velocity accordingly
        """
        self.applyBounceVector(
            CollisionHandler.rectBounceInsideRect(self.getCollider(), rect)
        )

    def bounce(self, rect: Rectangle):
        """
        Calls a collision detection between the instance's bounding box and another rectangle.
        Updates the instance's velocity vector from the collision response vector.
        """
        if CollisionHandler.rectCollidesOutsideRect(self.getCollider(), rect):
            self.applyBounceVector(
                CollisionHandler.rectBounceOutsideRect(
                    self.getCollider(), rect)
            )

    def collideInside(self, rect: Rectangle):
        """
        Calls an unelastic collision detection between the instance and another rectangle from inside.
        Updates the instance's velocity from the collision response vector.
        """
        self.applyMoveVector(
            CollisionHandler.rectCollideInertInsideRect(
                self.getCollider(), self.speedVector, rect
            )
        )

    def collideOutside(self, rect: Rectangle):
        """
        Calls an unelastic collision detection between the instance and another rectangle.
        Updates the instance's velocity from the collision response vector.
        """
        if (CollisionHandler.rectCollidesOutsideRect(self.getCollider(), rect)):
            self.applyMoveVector(
                CollisionHandler.rectCollideInertOusiteRect(
                    self.getCollider(), self.speedVector, rect
                )
            )

    def applyMoveRequestState(self):
        """
        Update the instance's velocity vector applying the move request state values.
        """
        moveRequest = self.moveRequestState
        self.applyMoveVector((
            -(moveRequest.left) | moveRequest.right,
            -(moveRequest.up) | moveRequest.down
        ))

    def update(self):
        """
        Updates the current animation frame and updates position.
        """
        self.animation.updateFrame()
        self.move()
