from setuptools import find_packages, setup

setup(
    name='kuadpyengine',
    packages=find_packages(include=['kuadpyengine']),
    version='0.1.0',
    description='A library providing some game programming features based on Pygame',
    author='Kuadrado Software',
    license='LGPL',
    install_requires=['pygame==2.0.1'],
    setup_requires=['pytest-runner'],
    tests_require=['pytest==6.2.2'],
    test_suite='tests'
)
